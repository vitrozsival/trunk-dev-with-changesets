# @trunk-dev-with-changesets/foo

## 0.2.2

### Patch Changes

- 637b4e6: Another hotfix needed.

## 0.2.1

### Patch Changes

- 8ad2e26: Some hotfix.

## 0.2.0

### Minor Changes

- da20a7a: Add new feature.

### Patch Changes

- @trunk-dev-with-changesets/bar@0.1.1

## 0.1.0

### Minor Changes

- 6a2c36a: Initial release

### Patch Changes

- Updated dependencies [6a2c36a]
  - @trunk-dev-with-changesets/bar@0.1.0
