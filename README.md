# PoC: Trunk-based development

This is a proof of concept for trunk-based development using [Changesets](https://github.com/changesets/changesets)
and [GitLab CI](https://docs.gitlab.com/ee/ci/).

## What is trunk-based development?

See [Trunk-based Development](https://trunkbaseddevelopment.com/).

## How does it work?

Unlike the traditional GitFlow, trunk-based development uses a single branch for all development. This branch is
called `main`. Changes are made on short-lived feature branches, and merged into `main` using pull requests as soon as
possible.

Releases (and hotfixes) are done by creating (again short-lived) release branches. These branches are used to prepare,
build and tag the release.

Once the release is tagged and all binaries are built, changes that are part of the Changesets process (deleting
released Changesets, creating changelogs etc.) are merged back into `main`.

## How does it differ from GitFlow?

- No long-lived branches
- Fewer merge conflicts
- Faster feedback
- Shorter code freeze periods during release

## Release Types

### Snapshot

Snapshots are used to test changes in a production-like environment. They are not meant to be used in production.
Each commit (PR) to `main` is automatically published as a snapshot. The pipeline output will tell you which versions
were created.

### Stable

Stable releases are meant to be used in production. They are created by diverting from `main` using a release branch.

```bash
git checkout main
git checkout -b release/next
git push -u origin release/next
```

When pushing the release branch, the CI pipeline will automatically build and trigger the release process. The pipeline
will wait for manual approval before creating the release in `stable-release` job. Once approved, the release will be
created, tagged, binaries built and published. The changes made on the release branch are then merged back to `main` and
existing release branch is automatically deleted.

Since each workspace in monorepo has its own version tag, the pipeline will also create one root tag for the entire
release with the following format:

```bash
"$CI_PROJECT_NAME@$(date -u +'%Y-%m-%dT%H-%M-%S')"
# e.g. trunk-dev-with-changesets@2024-02-05T14-00-00
```

### Hotfix

Hotfixes are used to fix critical issues in production. They are created by diverting from the latest stable release
tag.

```bash
git checkout trunk-dev-with-changesets@2024-02-05T14-00-00
git checkout -b hotfix/next
git push -u origin hotfix/next
```

When pushing the hotfix branch, the process is similar to the stable release. The pipeline will wait for manual approval
to create the hotfix, built the tagged binaries and publish them.

#### Important Notes on Hotfixes

##### Merging back to `main`

Changes on the hotfix branch **will not** be merged back to `main` automatically. This is to ensure there are
only relevant changes from the hotfix branch taken back into `main` (where the development continues) while resolving
potential conflicts. Once this is done, the hotfix branch can be deleted manually.

##### Versioning

Each commit to the hotfix branch triggers a new stable release with a relevant Changesets version. The CI will wait for
manual approval before creating the release, however, once you publish the hotfix, any new commit to the hotfix branch
**will not** create a release unless there are new Changesets added.
