# @trunk-dev-with-changesets/bar

## 0.1.1

### Patch Changes

- Updated dependencies [a628d6f]
  - @trunk-dev-with-changesets/baz@0.1.1

## 0.1.0

### Minor Changes

- 6a2c36a: Initial release

### Patch Changes

- Updated dependencies [6a2c36a]
  - @trunk-dev-with-changesets/baz@0.1.0
